const Settings = require('../models/Settings');
const auth = require('../auth.js');

module.exports.PostSettings = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const settingsData = req.body;

	if (userData.isAdmin) {
		try {
			const newSettings = await Promise.all(settingsData.map(async ({ key, value }) => {
				const settings = new Settings({ key, value });
				await settings.save();
			}));

			res.send('Successfully saved!');
		} catch (error) {
			console.error('Error creating settings', error);
			res.status(500).json({ error: 'Internal server error' });
		}
	}
};

/*Update Address*/
module.exports.EditAddress = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let updatedAddress = { value: req.body.value };
	if (userData.isAdmin) {
			Settings.findByIdAndUpdate(req.params.settingsId, updatedAddress)
			.then(result => res.send(`Address successfully updated!`))
			.catch(err => res.send(err))
	}
}

module.exports.getAddress = async (req, res) => {
  try {
    const result = await Settings.findOne({ key: "address" });
    return res.status(200).json(result);
  } catch (error) {
    console.error(error);
    return res.send(false);
  }
};


/*Get email*/
module.exports.GetEmail = (req, res) => {
		Settings.findOne({ key: 'email' })
		.then(result => res.send(result))
		.catch(err => res.send(err))
}

/*update email*/
module.exports.UpdateEmail = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let updatedEmail = { value: req.body.value };

	if (userData.isAdmin) {
		Settings.findByIdAndUpdate(req.params.emailId, updatedEmail)
		.then(result => res.send('Successfully updated'))
		.catch(err => res.send(err))
	}
}



/*get banner*/
module.exports.GetBanner = (req, res) => {
	Settings.findOne({ key: 'banner' })
	.then(result => res.send(result))
	.catch(err => res.send(err));
}

/*get benner-subtitle*/
module.exports.GetBannerSubtitle = (req, res) => {
	Settings.findOne({ key: 'banner-subtitle' })
	.then(result => res.send(result))
	.catch(err => res.send(err));
}

/*update banner*/
module.exports.UpdateBanner = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let updatedBanner = { value: req.body.value };

	if (userData.isAdmin) {
		Settings.findByIdAndUpdate( req.params.bannerId, updatedBanner)
		.then(result => res.send('Successfully updated'))
		.catch(err => res.send(err))
	}
}

/*update banner-subtitle*/
module.exports.UpdateBannerSubtitle = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let updatedBannerSubtitle = { value: req.body.value };

	if (userData.isAdmin) {
		Settings.findByIdAndUpdate( req.params.bannerSubtitleId, updatedBannerSubtitle)
		.then(result => res.send('Successfully updated'))
		.catch(err => res.send(err))
	}
}

module.exports.getAllSettings = (req, res) => {
	
		Settings.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
	
}

module.exports.getSettingsById = (req, res) => {
	Settings.findById(req.params.settingsId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.UpdateSettings = (req, res) => {
	let UpdatedSettings = { value: req.body.value };

	Settings.findByIdAndUpdate( req.params.settingsId, UpdatedSettings)
	.then(result => res.send(true))
	.catch(err => res.send(false))
}


module.exports
