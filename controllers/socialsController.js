const Socials = require('../models/Socials');
const auth = require('../auth.js');

module.exports.PostSocials = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const socialsData = req.body;

	if (userData.isAdmin) {
		try {
			const newSocialsData = await Promise.all(socialsData.map(async ({ key, value }) => {
				const socials = new Socials({ key, value });
				await socials.save();
			}));

			res.send('Successfully saved!');
		} catch (error) {
			console.error('Error creating settings', error);
			res.status(500).json({ error: 'Internal server error' });
		}
	}
};

module.exports.getAllSocialLinks = (req, res) => {
	
		Socials.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
	
}

module.exports.getSocialsById = (req, res) => {
	Socials.findById(req.params.socialId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.updateSocialLink = (req, res) => {
	let UpdatedSettings = { value: req.body.value };

	Socials.findByIdAndUpdate( req.params.socialId, UpdatedSettings)
	.then(result => res.send(true))
	.catch(err => res.send(false))
}

