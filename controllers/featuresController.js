const Features = require('../models/Features');
const auth = require('../auth.js');

/*add feature*/
module.exports.AddFeature = (req, res) => {
		const userData = auth.decode(req.headers.authorization);
		const { title, description, icon } = req.body;

		if (userData.isAdmin) {
				let newFeature = new Features({ title, description, icon });
				newFeature.save()
				.then(save => res.send(`Successfully saved`))
				.catch(err => res.send(err));
		}
}

/*get feature*/

module.exports.getFeatures = (req, res) => {
	Features.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateFeature = (req, res) => {

	let updatedFeature = {
		title : req.body.title,
		description : req.body.description,
		icon : req.body.icon
	}

	Features.findByIdAndUpdate(req.params.featureId, updatedFeature)
	.then(result => res.send(true))
	.catch(err => res.send(false))

}

module.exports.getFeatureById = (req, res) => {
	Features.findById(req.params.featureId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}