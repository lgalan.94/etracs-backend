const express = require('express');
const settingsController = require('../controllers/settingsController');
const auth = require('../auth.js');

const router = express.Router();

router.post('/add', auth.verify, settingsController.PostSettings);
router.get('/get-address', settingsController.getAddress);
router.get('/get-email', settingsController.GetEmail);
router.get('/get-banner', settingsController.GetBanner);
router.get('/get-banner-subtitle', settingsController.GetBannerSubtitle);
router.get('/settings-all', settingsController.getAllSettings)

router.patch('/:settingsId', settingsController.UpdateSettings);
router.get('/:settingsId', settingsController.getSettingsById);

router.put('/:emailId', auth.verify, settingsController.UpdateEmail);
router.put('/banner/:bannerId', auth.verify, settingsController.UpdateBanner);
router.put('/banner/:bannerSubtitleId', auth.verify, settingsController.UpdateBannerSubtitle);


module.exports = router;