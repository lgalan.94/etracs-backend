const express = require('express');
const featuresController = require('../controllers/featuresController');
const auth = require('../auth.js');

const router = express.Router();

router.post('/add-feature', auth.verify, featuresController.AddFeature);
router.get('/all', featuresController.getFeatures);

router.put('/:featureId', featuresController.updateFeature);
router.get('/:featureId', featuresController.getFeatureById);

module.exports = router;