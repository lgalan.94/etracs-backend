const express = require('express');
const socialsController = require('../controllers/socialsController');
const auth = require('../auth.js');

const router = express.Router();

router.post('/add', auth.verify, socialsController.PostSocials);
router.get('/get', socialsController.getAllSocialLinks);

router.patch('/:socialId', socialsController.updateSocialLink);
router.get('/:socialId', socialsController.getSocialsById);

module.exports = router;