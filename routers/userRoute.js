const express = require('express');
const userController = require('../controllers/userController');
const auth = require('../auth.js');

const router = express.Router();

//no params routes
router.post('/register-user', userController.registerUser);
router.post('/login', userController.userLogin);
router.get("/user-details", auth.verify, userController.retrieveUserDetails);

module.exports = router; 