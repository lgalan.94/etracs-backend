const mongoose = require('mongoose');

const featuresSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true,
		unique: true
	},

	description: {
		type: String,
		required: true
	},

	icon: {
		type: String,
		required:true
	}
})
const Features = mongoose.model('feature',featuresSchema);

module.exports = Features;