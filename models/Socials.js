const mongoose = require('mongoose');

const socialsSchema = new mongoose.Schema({
  key: {
    type: String,
    required: true,
    unique: true,
  },
  value: {
    type: String,
    required: true,
  },
});

const Socials = mongoose.model('Social', socialsSchema);

module.exports = Socials;