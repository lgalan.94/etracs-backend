const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	userName: { type: String, required: true },
	password: { type: String, required: true },
	isAdmin: { type: Boolean, default: false },
	createdOn: { type: Date, default: new Date }
})

const User = mongoose.model('User', userSchema);

module.exports = User;