const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoute = require('./routers/userRoute');
const settingsRoute = require('./routers/settingsRoute');
const featuresRoute = require('./routers/featuresRoute');
const socialsRoute = require('./routers/socialsRoute');

const app = express();
const port = 9000;

mongoose.connect("mongodb+srv://admin:admin@batch288galan.q03dxqw.mongodb.net/ETRACS-API?retryWrites=true&w=majority", { useNewUrlParser: true }, { useUnifiedToplogy: true });
const DBconn = mongoose.connection;
DBconn.on("error", console.error.bind(console, "Error database connection"));
DBconn.once("open", () => console.log('Successfully connnected to database'));

// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended:true }));

app.use(cors());
app.use('/user', userRoute);
app.use('/settings', settingsRoute);
app.use('/features', featuresRoute);
app.use('/socials', socialsRoute);

app.listen(port, () => console.log(`Server is running on port: ${port}`));